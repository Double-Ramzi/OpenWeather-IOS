import Foundation
import SharkORM
import OpenWeatherMapKit

class Location: SRKObject {
    @objc dynamic var name: String = "Troyes"
    @objc dynamic var selected: Int = 0

    static func setSelected(row: Int) -> Void {
        let locations = self.clearSelected()

        let selectedLocation = locations[row]
        selectedLocation.selected = 1
        selectedLocation.commit()

    }

    static func clearSelected() -> [Location] {
        let locations = Location.query().fetch() as! [Location]

        for location in locations {
            location.selected = 0
            location.commit()
        }

        return locations
    }

    static func getPicture(meteo: ForecastItem) -> String {
        switch meteo.weather[0].main {

        case "Clear":
            return "01d"
        case "Clouds":
            return "02d"
        case "":
            return "03d"
        case "":
            return "04d"
        case "":
            return "09d"
        case "Rain":
            return "10d"
        case "":
            return "11d"
        case "Snow":
            return "13d"
        case "":
            return "50d"
        default:
            dump(meteo.weather[0].main + " unknown !")

            return "01d"
        }
    }
}



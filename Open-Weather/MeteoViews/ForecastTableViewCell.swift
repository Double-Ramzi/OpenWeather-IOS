//
//  ForeCastTableViewCell.swift
//  Open-Weather
//
//  Created by Double Ramzi on 22/12/2018.
//  Copyright © 2018 Double Ramzi. All rights reserved.
//

import Foundation
import UIKit

class ForecastTableViewCell: UITableViewCell {
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var minTemp: UILabel!
    @IBOutlet weak var curTemp: UILabel!
    @IBOutlet weak var maxTemp: UILabel!
    @IBOutlet weak var date: UILabel!
}

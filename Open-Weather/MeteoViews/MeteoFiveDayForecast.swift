import Foundation
import UIKit
import OpenWeatherMapKit

class MeteoFiveDayForecast: UITableViewController {

    var meteosForecast: Array<ForecastItem>

    @IBOutlet weak var city: UILabel!

    required init?(coder aDecoder: NSCoder) {
        self.meteosForecast = Array<ForecastItem>()
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshForecast()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshForecast()
    }

    func refreshForecast() {
        let locations = Location
                .query()
                .where("selected = ?", parameters: [1])
                .fetch() as! [Location]

        let location: Location
        // Default to Troyes
        if (locations.first == nil) {
            location = Location()
            location.name = "Troyes"
        } else {
            location = locations.first!
        }
        dump("La location :")
        dump(location.name)
        self.city.text = location.name

        OpenWeatherMapKit.instance.weatherForecastForFiveDays(forCity: location.name) { (forecast, error) in
            if (forecast != nil) {
                self.meteosForecast = Array<ForecastItem>()
                for meteoForecast in forecast!.list {
                    self.meteosForecast.append(meteoForecast)
                }
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.reloadInputViews()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.meteosForecast.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celluleModule", for: indexPath) as! ForecastTableViewCell

        let meteo = self.meteosForecast[indexPath.row]
        cell.minTemp!.text = String(format: "%.1f°C", meteo.main.celsius.minTemp)
        cell.curTemp!.text = String(format: "%.1f°C", meteo.main.celsius.currentTemp)
        cell.maxTemp!.text = String(format: "%.1f°C", meteo.main.celsius.maxTemp)
        cell.date!.text = meteo.dt_txt!

        cell.picture.image = UIImage(named: Location.getPicture(meteo: meteo))

        return cell
    }
}

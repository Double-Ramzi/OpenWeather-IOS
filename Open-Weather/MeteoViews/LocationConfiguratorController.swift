import UIKit
import OpenWeatherMapKit

class LocationConfiguratorController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let locationNumber = Location
                .query()
                .fetch()
                .count
        dump(locationNumber)
        return locationNumber
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (Location.query().fetch() as! [Location])[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        Location.setSelected(row: row)
    }

    @IBOutlet weak var cityToAdd: UITextField!

    @IBOutlet weak var addCity: UIButton!

    @IBOutlet weak var currentCity: UIPickerView!
    
    @IBAction func addCity(_ sender: UIButton) {
        let cityToAdd = self.cityToAdd.text!

        let alreadyExist = Location
                .query()
                .where("name = ?", parameters: [cityToAdd])
                .fetch()
                .count
                >= 1
        if (!alreadyExist) {
            let locations = Location.clearSelected()

            let newLocation = Location()
            newLocation.name = cityToAdd
            newLocation.selected = 1
            newLocation.commit()
            Utils.showToast(message: cityToAdd + " added!", controller: self)
            let lastCity = Location
                    .query()
                    .fetch()
                    .count

            self.currentCity.reloadAllComponents()

        } else {
            Utils.showToast(message: cityToAdd + " already exist!", controller: self)
        }


    }


}
